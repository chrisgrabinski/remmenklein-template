'use strict';

var p               = require('./package.json'),
    gulp            = require('gulp'),
    $               = require('gulp-load-plugins')(),
    mainBowerFiles  = require('main-bower-files'),
    pngquant        = require('imagemin-pngquant');

var src             = 'src/',
    dist            = 'dist/';

var filterByExtension = function(extension){
    return $.filter(function(file){
    return file.path.match(new RegExp('.' + extension + '$'));
    });
};

// Default Task
gulp.task('default', function() {
    gulp.start(['build']);
});

// Images Task
gulp.task('images', function () {
    // Todo: Add imagemin
    gulp.src('src/images/*')
    .pipe($.imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
    .pipe(gulp.dest(dist + '/images'))
    .pipe($.size())
    .pipe($.connect.reload());
});

// Styles Task
gulp.task('styles', function() {
  return gulp.src('src/styles/*.scss')
        .pipe($.sourcemaps.init()) // Initiate sourcemaps
        .pipe($.sass()) // Compile SASS
        .pipe($.autoprefixer({ // Autoprefix CSS
            browsers: ['IE 8', 'IE 9', 'last 2 versions'],
            cascade: false
        }))
        .pipe($.minifyCss({ // Minify CSS, keep Wordpress theme comment
          keepSpecialComments: 1
        }))
        .pipe($.sourcemaps.write('.')) // Write sourcemaps
        .pipe(gulp.dest(dist + '/styles'))
        .pipe($.size())
        .pipe($.connect.reload());
});

// Scripts Task
gulp.task('scripts', function() {
  gulp.src('src/scripts/*.js')
  .pipe($.uglify())
  .pipe(gulp.dest(dist + '/scripts'))
  .pipe($.size())
  .pipe($.connect.reload());
});

// Bower Task
// Concat main files into vendor.js
gulp.task('bower', function() {

  var mainFiles = mainBowerFiles();

  if(!mainFiles.length){
    // No main files found. Skipping....
    return;
  }

  return gulp.src(mainFiles)
         .pipe(filterByExtension('js'))
         .pipe($.concat('vendor.js'))
         .pipe($.uglify())
         .pipe(gulp.dest(dist + '/scripts'))
         .pipe($.size())
         .pipe(jsFiles.restore())
         .pipe(filterByExtension('css'))
         .pipe($.concat('vendor.css'))
         .pipe(gulp.dest(dist + '/styles'))
         .pipe($.size());
});

// HTML Task
gulp.task('html', function() {
  gulp.src('src/*.html')
  .pipe(gulp.dest(dist))
  .pipe($.size())
  .pipe($.connect.reload());
});

// Extras Task
gulp.task('extras', function() {
  gulp.src('src/*.txt')
  .pipe(gulp.dest(dist))
  .pipe($.size());
});

// Connect
// Web-Server

gulp.task('connect', function() {
  $.connect.server({
    root: 'dist',
    livereload: true
  });
});


// Watch Task
gulp.task('watch', ['connect'], function () {
  gulp.watch(['src/*.html'], ['html']);
  gulp.watch(['src/styles/**/*.{scss,css}'], ['styles']);
  gulp.watch(['src/scripts/**/*.js'], ['scripts', 'bower']);
  gulp.watch(['src/images/**/*'], ['images']);
});

// Build Task
gulp.task('build', ['styles', 'scripts', 'html', 'images', 'extras', 'bower']);
